/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;



/**
 *
 * @author VirtualXI
 */
public class ArrayList<E> implements List {
    final int SIZE = 16;
    int size;
    E[] data;
    int numElems = 0;
    int increment = 0;

    ArrayList() {
        data = (E[]) new Object[SIZE];
        increment = 16;
        size = SIZE;
    }

    ArrayList(int size) {
        increment = 16;
        data = (E[]) new Object[size];
    }

    ArrayList(int size, int increment) {
        data = (E[]) new Object[size];
        this.increment = increment;
    }

    /*
     * public void add(T element) { if (size == numElems) { increaseDataSize(6); }
     * data[numElems] = element; numElems++; System.out.println(numElems + "+" +
     * size); }
     */
    @Override
    public E get(int index) {
        return data[index];
    }

    public void increaseDataSize(int n) {

        E[] result = (E[]) new Object[size + n];
        for (int i = 0; i < data.length; i++) {
            result[i] = data[i];
        }
        data = result;
        size = size + n;
    }

    public void print() {
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean contains(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Iterator iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object[] toArray(Object[] a) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean add(Object e) {
        if (size == numElems) {
            increaseDataSize(increment);
        }
        data[numElems] = (E) e;
        numElems++;
        //System.out.println(numElems + "+" + size);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void clear() {
        // TODO Auto-generated method stub

    }

    @Override
    public Object set(int index, Object element) {

        E oldValue = this.data[index];
        data[index] = (E) element;
        return oldValue;
    }

    @Override
    public void add(int index, Object element) {
        E aux;
        for (int i = numElems-1; i >= index; i--) {
            
            aux=data[i];
            data[i+1]=aux;

        }
        data[index]=(E)element;
    }

    @Override
    public Object remove(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int indexOf(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        // TODO Auto-generated method stub
        return null;
    }

}
